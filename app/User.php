<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\InformationUser;
use App\Notifications\VerifyEmail;
class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'confirmation_code', 'confirmed'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function infor_user(){
        return $this->hasOne('App\InformationUser','id_user','id');
    }
    public function isAdmin(){
        $id_user_currency = $this->id;
        $confirmed = $this->confirmed;
        if($confirmed == 1){
            $user_infor = InformationUser::where('id_user', $id_user_currency)->first();
            if($user_infor->id_Role == 0) {
                return true;
            } else {
                return false;
            }
        }
    }
    public function isSubscribe(){
        $id_user_currency = $this->id;
        $confirmed = $this->confirmed;
        if($confirmed == 1){
            $user_infor = InformationUser::where('id_user', $id_user_currency)->first();
            if($user_infor->id_Role == 3) {
                return true;
            } else {
                return false;
            }
        }
    }
    public function isPartner(){
        $id_user_currency = $this->id;
        $confirmed = $this->confirmed;
        if($confirmed == 1){
            $user_infor = InformationUser::where('id_user', $id_user_currency)->first();
            if($user_infor->id_Role == 2) {
                return true;
            } else {
                return false;
            }
        }
    }
}
