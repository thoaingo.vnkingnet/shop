<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\InformationUser;
use App\Category;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
class AdminController extends Controller
{
    public function __construct(){
        
    }
	public function getDashboard(){
        return view('admin.pages.dashboard');
    }
    public function getLogin(){
        return view('admin.pages.login');

    }
    public function postLogin(Request $request){
        $this->validate($request,
            [
                'email' => 'required',
                'password' => 'required'
            ],
            [
                'email.required' => 'Username or Mail is requied',
                'password.required' => 'Password is required',

            ]
        );
        $remember = $request->input('remember_me');

        if(Auth::attempt(['email' => $request->email,'password'=> $request->password], $remember)){

            if((Auth::user()["original"]["id_Role"])==1){
                return redirect()->route('gnut-dashboard');
            } else {
                return redirect()->route('gnut-dashboard');
            }
            
        }else{
                return back()->with('thongbao' , 'Đăng nhập sai rồi vui lòng nhập lại!!!');
        }
        return view('admin.pages.login');
    }
    public function getLogout(){
        Auth::logout();
        return redirect()->route('gnut-login');
    }
    public function getSignup(){
        return view('admin.pages.signup');
    }
    public function postSignup(Request $req){
        $email = $req->email;
        $username = $req->username;
        $this->validate($req,
            [
                'username' => ['required', 'string','min:3', 'max:255'],
                'firstname' => ['max:255'],
                'lastname' => ['max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
                'confirm' => ['required'],
            ],
            [
                'username.required' =>'Username is required',
                'username.min' =>'Minimum username 3 characters',
                'username.max' =>'Maximum username 255 characters',
                'email.required' =>'Mail is required',
                'email.unique' =>'Mail is exist',
                'email.email' =>'Email format is incorrect',
                'password.min' =>'Password must be 8 characters or more',
                'password.required' =>'You must enter a password',
                'confirm.required' =>'You need confirm',
        ]);
        $confirmation_code = time().uniqid(true);

        $user = User::create([
            'name' => $req->username,
            'email' => $req->email,
            'password' => bcrypt($req->password),
            'confirmation_code' => $confirmation_code
        ]);
        InformationUser::create([
            'id_user' => $user->id,
            'firstname' => $req->firstname,
            'lastname' => $req->lastname,
            'address' => '',
            'phone' => '',
            'country' => '',
            'avatar' => '',
            'id_Role' => 0,
        ]);
        Mail::send('admin.email.verify', ['confirmation_code' => $confirmation_code], function($message) use($req) {
            $message
                ->to($req->email,$req->username)
                ->subject('Verify your email address');
        });

        return redirect(route('gnut-signup'))->with('success','Thanks for signing up! Please check your email.');
    }

    public function confirm_signup($confirmation_code){
        $user = User::where('confirmation_code', $confirmation_code)->first();
        if(!$user){
            return redirect(route('gnut-signup'))->with('error','Can not found verified your account.');
        }
        $user->confirmed = 1;
        $user->save();
        return redirect(route('gnut-signup'))->with('success','You have successfully verified your account.');
    }

    /*Post*/
    public function getPostnew(){
        return view('admin.post.new-post');
    }
    public function getAllpost(){
        return view('admin.post.all-post');
    }


    /*Category post*/
    public function getCategorypost(){
        $data['category'] = Category::with('parent')->get();
        return view('admin.post.category' , $data);
    }
    public function postCategorypost(Request $req){
        $category = new category;
        $this->validate($req,
            [
                'category' => 'required|unique:category,title|min:3',
            ],
            [
                'category.required' =>'Please enter category',
                'category.unique' =>'Category is exists',
                
            ]);
          
        $category->title = strip_tags($req->category);
        $category->slug = Str::slug($req->category, '-');
        $category->parent_id = strip_tags($req->parent_id);
        $category->desciption = 'public';
        $category->status = 'public';
        $category->save();
        
        return redirect()->route('getCategorypost')->with('success','Add category success');
    }
    public function editgetcategory($slug,$id){
        $data['category_edit'] = category::find($id);
        $data['category'] = category::with('parent')->where('id', '!=', $id)->get();
        return view('admin.post.edit-category' , $data);
    }
    public function editpostcategory(Request $red,$id){
         $this->validate($req,
            [
                'category' => 'required|unique:category,title|min:3',
            ],
            [
                'category.required' =>'Please enter category',
                'category.unique' =>'Category is exists',
                
            ]);
        $category = category::find($id);
        $category->title = strip_tags($req->category);
        $category->slug = Str::slug($req->category, '-');
        $category->parent_id = strip_tags($req->parent_id);
        $category->save();
        return redirect()->route('editgetcategory',['slug' => $category->slug, 'id' => $id])->with('success','Edit category success');
    }
    public function getDeleteCategory($id){
        $delete_category = category::find($id);
        $delete_category->delete();
        return redirect()->route('getCategorypost')->with('success','Delete category success');
    }
    


    

    public function getAddAccount(){
        return view('admin.add-user');
    }
    
}
