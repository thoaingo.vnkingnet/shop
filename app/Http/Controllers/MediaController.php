<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Media;

class MediaController extends Controller
{
    public function getMedia(){
		$files = Media::orderBy('created_at','DESC')->paginate(12);
		//$files = Media::orderBy('created_at','DESC')->simplePaginate(9);
    	return view('admin.pages.media',['files' => $files]);
    }

    public function create(){
    	return view('admin.pages.media_upload');
    }

    public function dropzone(Request $request){
        $this->validate($request,[
            'file' => 'required|file|max:20000'
        ]);
    	$upload = $request->file('file');
    	$path = $upload->store('public/storage');
    	Media::create([
    		'title' => $upload->getClientOriginalName(),
    		'description' => '',
    		'path' => $path,
    	]);
    	return redirect(route('viewfile'))->with(['success'=> 'File is uploaded']);
    }

    public function store(Request $request){
    	$this->validate($request,[
    		'file' => 'required|file|max:20000'
    	]);
    	$upload = $request->file('file');
    	$path = $upload->store('public/storage');
    	$file = Media::create([
    		'title' => $upload->getClientOriginalName(),
    		'description' => '',
    		'path' => $path,
    	]);
    	return redirect(route('viewfile'))->with(['success'=> 'File is uploaded']);
    }
    public function show($id){

    }

    public function destroymedia($id){
        // die();
    	$del = Media::find($id);
    	Storage::delete($del->path);
    	$del->delete();
    	// return redirect()->back();
    	return redirect(route('viewfile'))->with('success','Delete success!');
    }
}
