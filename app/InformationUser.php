<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InformationUser extends Model
{
    protected $table = "infor_user";
    protected $fillable =[
    	'firstname', 'lastname', 'address', 'phone', 'country', 'avatar', 'id_Role', 'id_user'
    ];
    public function user(){
    	return $this->belongsTo('App\User','id_user','id');
    }
}
