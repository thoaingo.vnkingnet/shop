<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $table = 'category';
    public $guard = [];
    // public function posts(){
    // 	 return $this->hasMany('App\posts','idCat','id');
    // }
  
    public function parent()
    {
        return $this->belongsTo('App\Category', 'parent_id', 'id');
    }
    public function children()
    {
        return $this->hasMany('App\Category', 'parent_id','id');
    }
}
