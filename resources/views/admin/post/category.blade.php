@extends('admin.layouts.backend')
@section('title')
Category
@endsection
@section('css_before')

@endsection
@section('css_after')

@endsection
@section('content')
<!-- Page Content -->
<div class="block block-rounded block-bordered">
    <div class="block-header block-header-default">
        <h3 class="block-title">Category</h3>
    </div>
    <div class="block-content block-content-full">
        <div class="row">
            <div class="col-lg-4">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Add category</h3>
                    </div>
                    <div class="block-content">
                        <div class="row">
                            <div class="col-md-12">
                                @if(session('success'))
                                    <div class="alert alert-success alert-dismissable" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        <p class="mb-0">{{session('success')}}</p>
                                    </div>
                                @endif
                                @if(count($errors)>0)
                                <div class="alert alert-danger alert-dismissable" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <p class="mb-0">
                                        @foreach($errors->all() as $err)
                                            {{$err}}<br>
                                        @endforeach
                                    </p>
                                </div>
                                    
                                @endif
                                <form class="form-horizontal" action="{{route('postCategorypost')}}" enctype="multipart/form-data" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="category">Category Name</label>
                                        <input type="text" class="form-control" id="category" name="category" placeholder="Enter category">
                                    </div>
                                    <div class="form-group">
                                        <label for="block-form-password">Parent</label>
                                        <?php
                                        function cat_select($category, $parent_id = 0, $char = '') {    foreach ($category as $key => $item)
                                                    {
                                                        if ($item['parent_id'] == $parent_id)
                                                        {
                                                            echo '<option value="'.$item['id'].'">';
                                                                echo $char . $item['title'];
                                                            echo '</option>';
                                                         cat_select($category, $item['id'], $char.' -  ');
                                                        }
                                                    }
                                            }
                                            echo '<select name="parent_id" class="form-control">';
                                            echo '<option value="0">Choose parent category</option>';
                                            echo cat_select($category);
                                            echo '</select>';
                                        ?>
                                        
                                    </div>
                                    <div class="block-content block-content-full block-content-sm  text-center">
                                          <button type="submit" class="btn btn-primary">Add</button>
                                          <button type="reset" class="btn">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">List category</h3>
                    </div>
                    <div class="block-content">
                        <!-- If you put a checkbox in thead section, it will automatically toggle all tbody section checkboxes -->
                        <table class="js-table-checkable table table-hover table-vcenter js-table-checkable-enabled">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 70px;">
                                        <div class="custom-control custom-checkbox custom-control-primary d-inline-block">
                                            <input type="checkbox" class="custom-control-input" id="check-all" name="check-all">
                                            <label class="custom-control-label" for="check-all"></label>
                                        </div>
                                    </th>
                                    <th>Name</th>
                                    <th class="d-none d-sm-table-cell" style="width: 15%;">Slug</th>
                                    <th class="d-none d-sm-table-cell" style="width: 20%;">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    function categorylist($categorylist, $parent_idd = 0, $char = '')
                                        {
                                        foreach ($categorylist as $key => $cat)
                                            {
                                                // Nếu là chuyên mục con thì hiển thị
                                                if ($cat['parent_id'] == $parent_idd)
                                                {?>
                                                <tr>
                                                    <td class="text-center">
                                                        <div class="custom-control custom-checkbox custom-control-primary d-inline-block">
                                                            <input type="checkbox" class="custom-control-input" id="checked" name="checked" value="{{$cat->id}}">
                                                            <label class="custom-control-label" for="checked"></label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <p class="font-w600 mb-1">
                                                            <a href="{{ route('editgetcategory',[ 'slug' => $cat->slug ,'id' => $cat->id]) }}">
                                                                <?php echo $char;?>{{$cat->title}}
                                                            </a>
                                                        </p>
                                                    </td>

                                                    <td><?php echo $char;?>{{$cat->slug}}</td>
                                                    <td class="center ">
                                                        <a class="btn btn-info"  href="{{ route('editgetcategory',['slug' => $cat->slug, 'id' => $cat->id]) }}">
                                                            <i class="fas fa-fw fa-pencil-alt"></i>  
                                                        </a>
                                                        <a class="btn btn-danger blockU"  href="{{route('delete-category',['id' => $cat->id])}}" onclick='confirmDelete()'>
                                                            <i class="far fa-trash-alt"></i>
                                                        </a>
                                                    </td>
                                                </tr>   
                                                <?php 
                                                    // Xóa chuyên mục đã lặp
                                                    unset($categorylist[$key]);
                                                    // Tiếp tục đệ quy để tìm chuyên mục con của chuyên mục đang lặp
                                                    categorylist($categorylist, $cat['id'], $char.' --  ');
                                                }
                                            }
                                        }
                                    echo categorylist($category);
                                ?>

                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END Page Content -->
@endsection

@section('js_after')

<script>jQuery(function(){ Dashmix.helpers(['table-tools-checkable', 'table-tools-sections']); });</script>
@endsection
