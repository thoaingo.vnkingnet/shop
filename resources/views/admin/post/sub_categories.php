<option value="{{ $sub_categories->id }}">{{ $sub_categories->title }}</option>
@if ($sub_categories->categories)
    @if(count($sub_categories->categories) > 0)
        @foreach ($sub_categories->categories as $subCategories)
            @include('admin.post.sub_categories', ['sub_categories' => $subCategories])
        @endforeach
    @endif
@endif