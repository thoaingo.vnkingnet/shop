@extends('admin.layouts.backend')
@section('title')
Upload file
@endsection
@section('css_before')
<link rel="stylesheet" href="{{ url('admin/js/plugins/magnific-popup/magnific-popup.css') }}">
@endsection
@section('content')
<div class="content">
    <h2 class="content-heading">Advanced</h2>
    @if(session('success'))
        <div class="alert alert-success alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <p class="mb-0">{{session('success')}}</p>
        </div>
    @endif
    <div class="row items-push js-gallery">
        @foreach($files as $file)
        <div class="col-md-6 col-lg-4 col-xl-3 animated fadeIn">
            <div class="options-container fx-item-zoom-in fx-overlay-zoom-out">
                <img class="img-fluid options-item" style="height: 250px;object-fit: cover;width: 100%;" src="{{Storage::url($file->path)}}" alt="">
                <div class="options-overlay bg-black-75">
                    <div class="options-overlay-content">
                        <a class="btn btn-sm btn-primary img-lightbox" href="{{Storage::url($file->path)}}">
                            <i class="fa fa-search-plus mr-1"></i> View
                        </a>
                        <a class="btn btn-sm btn-secondary" href="javascript:void(0)">
                            <i class="fa fa-pencil-alt mr-1"></i> Edit
                        </a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    {{ $files->links() }}
</div>
@endsection

@section('js_after')

<script src="{{ url('admin/js/plugins/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
<script>jQuery(function(){ Dashmix.helpers('magnific-popup'); });</script>
@endsection
