@extends('admin.layouts.backend')
@section('title')
Upload file
@endsection
@section('css_before')
<link rel="stylesheet" href="{{ url('admin/js/plugins/magnific-popup/magnific-popup.css') }}">
<link rel="stylesheet" href="{{ url('admin/js/plugins/dropzone/dist/dropzone.css') }}">
@endsection
@section('content')
<div class="block block-rounded block-bordered">
    <div class="block-header block-header-default">
        <h3 class="block-title">Upload file</h3>
    </div>
    <div class="block-content block-content-full">
        <h2 class="content-heading">Asynchronous File Uploads</h2>
        <div class="row">
            <div class="col-lg-4">
                <p class="text-muted">
                    Drag and drop sections for your file uploads
                </p>
            </div>
            <div class="col-lg-8 col-xl-5">
                <!-- DropzoneJS Container -->
                <form action="{{route('upload_dropzone')}}" class="dropzone" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="fallback">
                        <input name="file" type="file" />
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js_after')

<script src="{{ url('admin/js/plugins/dropzone/dropzone.min.js') }}"></script>
@endsection
