<!-- Side Navigation -->
<div class="content-side content-side-full">
    <ul class="nav-main">
        <li class="nav-main-item">
            <a class="nav-main-link{{ request()->is('admin/dashboard') ? ' active' : '' }}" href="/admin/dashboard">
                <i class="nav-main-link-icon si si-cursor"></i>
                <span class="nav-main-link-name">Dashboard</span>
                <span class="nav-main-link-badge badge badge-pill badge-success">5</span>
            </a>
        </li>
        <li class="nav-main-heading">CONTENT</li>
        <li class="nav-main-item{{ request()->is('admin/post/*') ? ' open' : '' }}">
            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
                <i class="nav-main-link-icon fas fa-thumbtack"></i>
                <span class="nav-main-link-name">Post</span>
            </a>
            <ul class="nav-main-submenu">
                <li class="nav-main-item">
                    <a class="nav-main-link{{ request()->is('admin/post/edit-post') ? ' active' : '' }}" href="{{route('post-new')}}">
                        <span class="nav-main-link-name">All post</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link{{ request()->is('admin/post/add') ? ' active' : '' }}" href="{{route('post-new')}}">
                        <span class="nav-main-link-name">Add post</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link{{ request()->is('admin/post/category') ? ' active' : '' }}" href="{{url('admin/post/category')}}">
                        <span class="nav-main-link-name">Category</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link{{ request()->is('admin/post/category') ? ' active' : '' }}" href="{{url('admin/post/category')}}">
                        <span class="nav-main-link-name">Tags</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-main-item{{ request()->is('admin/page/*') ? ' open' : '' }}">
            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
                <i class="nav-main-link-icon fas fa-file"></i>
                <span class="nav-main-link-name">Page</span>
            </a>
            <ul class="nav-main-submenu">
                <li class="nav-main-item">
                    <a class="nav-main-link{{ request()->is('admin/page/edit-page') ? ' active' : '' }}" href="{{url('admin/page/edit-page')}}">
                        <span class="nav-main-link-name">All page</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link{{ request()->is('admin/page/add-page') ? ' active' : '' }}" href="{{url('admin/page/add-page')}}">
                        <span class="nav-main-link-name">Add page</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-main-item{{ request()->is('admin/manager-media*') ? ' open' : '' }}">
            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
                <i class="nav-main-link-icon fas fa-camera"></i>
                <span class="nav-main-link-name">Media</span>
            </a>
            <ul class="nav-main-submenu">
                <li class="nav-main-item">
                    <a class="nav-main-link{{ request()->is('admin/manager-media') ? ' active' : '' }}" href="{{route('viewfile')}}">
                        <span class="nav-main-link-name">Library</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link{{ request()->is('admin/manager-media/upload') ? ' active' : '' }}" href="{{route('formuploadfile')}}">
                        <span class="nav-main-link-name">Add Media</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-main-heading">BOOKING</li>
        <li class="nav-main-item{{ request()->is('admin/location/*') ? ' open' : '' }}">
            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
                <i class="nav-main-link-icon fas fa-map-marker-alt"></i>
                <span class="nav-main-link-name">Location</span>
            </a>
            <ul class="nav-main-submenu">
                <li class="nav-main-item">
                    <a class="nav-main-link{{ request()->is('admin/location') ? ' active' : '' }}" href="{{url('admin/location')}}">
                        <span class="nav-main-link-name">All Location</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link{{ request()->is('admin/location/add') ? ' active' : '' }}" href="{{url('admin/add-location')}}">
                        <span class="nav-main-link-name">Add Location</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-main-item{{ request()->is('admin/tours/*') ? ' open' : '' }}">
            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
                <i class="nav-main-link-icon si si-flag"></i>
                <span class="nav-main-link-name">Tours</span>
            </a>
            <ul class="nav-main-submenu">
                <li class="nav-main-item">
                    <a class="nav-main-link{{ request()->is('admin/tours') ? ' active' : '' }}" href="{{url('admin/location')}}">
                        <span class="nav-main-link-name">All Tours</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link{{ request()->is('admin/tours/add') ? ' active' : '' }}" href="{{url('admin/add-location')}}">
                        <span class="nav-main-link-name">Add Tour</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link{{ request()->is('admin/tours/category') ? ' active' : '' }}" href="{{url('admin/add-location')}}">
                        <span class="nav-main-link-name">Category</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link{{ request()->is('admin/tours/attribute') ? ' active' : '' }}" href="{{url('admin/add-location')}}">
                        <span class="nav-main-link-name">Attributes</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link{{ request()->is('admin/tours/availability') ? ' active' : '' }}" href="{{url('admin/add-location')}}">
                        <span class="nav-main-link-name">Availability</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link{{ request()->is('admin/tours/booking-calendar') ? ' active' : '' }}" href="{{url('admin/add-location')}}">
                        <span class="nav-main-link-name">Booking Calendar</span>
                    </a>
                </li>
            </ul>
        </li>

        <li class="nav-main-heading">More</li>
        <li class="nav-main-item">
            <a class="nav-main-link" href="/">
                <i class="nav-main-link-icon si si-globe"></i>
                <span class="nav-main-link-name">Landing</span>
            </a>
        </li>
    </ul>
</div>
<!-- END Side Navigation -->