<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

Auth::routes([
  'register' => false,
  'verify' => true,
  'reset' => false
]);
Route::group(['prefix' => 'admin' , 'middleware' => 'CheckAdmin'], function(){

	Route::get('logout','AdminController@getLogout')->name('gnut-logout');
	Route::get('dashboard', 'AdminController@getDashboard')->name('gnut-dashboard');
	Route::get('manager-media', 'MediaController@getMedia')->name('viewfile');
	Route::get('manager-media/upload', 'MediaController@create')->name('formuploadfile');
	Route::post('manager-media/upload', 'MediaController@dropzone')->name('upload_dropzone');
	Route::get('manager-media-delete/{id}',
		['as' => 'manager-media-delete',
		'uses'=>'MediaController@destroymedia'
	]);
	Route::get('post/add',
		['as' => 'post-new',
		'uses'=>'AdminController@getPostnew'
	]);
	/*Category*/
	Route::get('post/category',
	['as' => 'getCategorypost',
		'uses'=>'AdminController@getCategorypost'
	]);
	Route::post('post/category',
	['as' => 'postCategorypost',
	'uses'=>'AdminController@postCategorypost'
	]);
	Route::get('category/{slug}/editcategory/{id}',
		['as' => 'editgetcategory',
		'uses'=>'AdminController@editgetcategory'
		]);
	Route::post('category/{id}/editcategory',
		['as' => 'editpostcategory',
		'uses'=>'AdminController@editpostcategory'
		]);
	Route::get('delete-category/{id}',
		['as' => 'delete-category',
		'uses'=>'AdminController@getDeleteCategory'
	]);
});

/*Login - SignUp admin*/
Route::group(['prefix' => 'gnut-login','middleware' => 'CheckLogin'], function(){
	Route::get('/', 'AdminController@getLogin')->name('gnut-login');
	Route::post('/',
		[
		'as' => 'gnut-postlogin',
		'uses'=>'AdminController@postLogin'
		]
	);
});

Route::group(['prefix' => 'gnut-signup','middleware' => 'CheckLogin'], function(){
	Route::get('/', 'AdminController@getSignup')->name('gnut-signup');
	Route::post('/', 'AdminController@postSignup')->name('gnut-post-signup');
	Route::get('verify/{confirmationCode}', [
	    'as' => 'confirmation_path',
	    'uses' => 'AdminController@confirm_signup'
	]);
});
